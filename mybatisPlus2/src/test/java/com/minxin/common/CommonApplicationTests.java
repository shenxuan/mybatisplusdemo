package com.minxin.common;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.minxin.modules.bz.entity.People;
import com.minxin.modules.bz.entity.Student;
import com.minxin.modules.bz.service.PeopleService;
import com.minxin.modules.bz.service.StudentService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

/**
 * 基本查询
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class CommonApplicationTests {


	@Autowired
	private StudentService studentService;
	@Autowired
	private PeopleService peopleService;


	/**
	 * 保存
	 */
	@Test
	public void insert() {
		Student studentEntity = Student.builder().age(1).studentName("mik").build();
		studentService.insert(studentEntity);
	}

	/**
	 * 查询单条记录
	 */
	@Test
	public void getOne() {
        /*
        SELECT id,name,age,email,manager_id,create_time FROM people WHERE age > ? ;
        2.x 版本 查询出多条-报错。
        3.x service封装过
        (Caused by: org.apache.ibatis.exceptions.TooManyResultsException: Expected one result (or null) to be returned by selectOne(), but found: 2)
         */
		//People people = peopleService.getOne(Wrappers.<People>lambdaQuery().gt(People::getAge, 20));
		//People age1 = peopleDao.selectOne(new QueryWrapper<People>().gt("age", 20));
		People age = peopleService.selectOne(new EntityWrapper<People>().gt("age", 5));
		log.info(" IService getOne - " + age);
	}

	/**
	 * 使用select只查询指定字段
	 */
	@Test
	public void selectTest() {
		EntityWrapper<People> queryWrapper = new EntityWrapper<>();
        /*

        SELECT id,name,age,email FROM people WHERE age IN (?,?,?,?) limit 1
         */
		queryWrapper.in("age", Arrays.asList(30, 31, 34, 18)).last("limit 1")
				.setSqlSelect("age");

		List<People> peopleList = peopleService.selectList(queryWrapper);
		peopleList.forEach(System.out::print);
	}


	/**
	 * 拼接sql
	 * SELECT id,name,age,email,manager_id,create_time FROM bz_people WHERE DATE_FORMAT(create_time,'%Y-%m-%d')=?
	 */
	@Test
	public void addFilterTest() {
		//拼接sql

		List<People> list = peopleService.selectList(new EntityWrapper<People>()
				.addFilter("DATE_FORMAT(create_time,'%Y-%m-%d')={0}", "2019-07-30"));
		list.forEach(System.out::println);
	}

	/**
	 * 批量保存
	 */
	@Test
	public void saveBatch() {
		People people1 = People.builder().name("batch1").age(19).email("792171677@qq.com").build();
		People people2 = People.builder().name("batch2").age(20).email("792171678@qq.com").build();

        /*
        DEBUG==>  Preparing: INSERT INTO people ( name, age, email ) VALUES ( ?, ?, ? )
        DEBUG==> Parameters: batch1(String), 19(Integer), 792171677@qq.com(String)
        DEBUG==> Parameters: batch2(String), 20(Integer), 792171678@qq.com(String)
         */
		List<People> peopleList = Arrays.asList(people1, people2);
		boolean saveBatch = peopleService.insertBatch(peopleList);
		log.info(" IService saveBatch - " + saveBatch);
	}


	/**
	 * 更新null值
	 */
	@Test
	public void updateNull() {
		People people = peopleService.selectById(1L);
		people.setEmail(null);
		peopleService.updateAllColumnById(people);
	}


}
