package com.minxin.common;

import com.minxin.modules.bz.entity.People;
import com.minxin.modules.bz.service.PeopleService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 乐观锁测试
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class OptLockerTests {

	@Autowired
	private PeopleService peopleService;

	@Test
	public void insertTestSuccess() {
		People people = People.builder().age(10).name("乐观锁测试").version(1).build();
		peopleService.insert(people);

		people = new People();
		people.setId(people.getId());
		people.setVersion(0);
		people.setAge(11);
		//若版本号相同，则更新。否则更新失败
		boolean result = peopleService.updateById(people);
		System.out.println(result);

	}


}
