package com.minxin.common;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.minxin.modules.bz.entity.Manager;
import com.minxin.modules.bz.entity.People;
import com.minxin.modules.bz.service.ManagerService;
import com.minxin.modules.bz.service.PeopleService;
import com.minxin.modules.bz.service.StudentService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 多表联合查询测试
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class JoinQueryTests {

	@Autowired
	private StudentService studentService;

	@Autowired
	private PeopleService peopleService;

	@Autowired
	private ManagerService managerService;

	@Test
	public void insertTest() {
		Manager m = Manager.builder().name("管理者6").managerAge(12).build();
		managerService.insert(m);

		People p1 = People.builder().age(2).name("p2").email("123@q.com").managerId(m.getId()).build();
		peopleService.insert(p1);
	}

	/**
	 * 多表查询并分页排序
	 */
	@Test
	public void testQuery() {

		Page<People> result = peopleService.selectVoList(new Page<>(1, 2),
				(EntityWrapper<People>) new EntityWrapper<People>()
						.ge("p.id", 1)
						.orderBy("m.id", false));


		result.getRecords().forEach(System.out::println);


	}


}
