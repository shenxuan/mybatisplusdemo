package com.minxin.modules.bz.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.minxin.modules.bz.dao.StudentDao;
import com.minxin.modules.bz.entity.Student;
import com.minxin.modules.bz.service.StudentService;
import org.springframework.stereotype.Service;

@Service
public class StudentServiceImpl extends ServiceImpl<StudentDao, Student> implements StudentService {
}
