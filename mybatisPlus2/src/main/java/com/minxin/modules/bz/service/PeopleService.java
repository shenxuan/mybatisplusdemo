
package com.minxin.modules.bz.service;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.minxin.modules.bz.entity.People;

public interface PeopleService extends IService<People> {

	Page<People> selectVoList(Page<People> peoplePage, EntityWrapper<People> peopleQueryWrapper);

}
