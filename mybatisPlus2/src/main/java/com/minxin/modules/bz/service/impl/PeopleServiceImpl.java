package com.minxin.modules.bz.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.minxin.modules.bz.dao.PeopleDao;
import com.minxin.modules.bz.entity.People;
import com.minxin.modules.bz.service.PeopleService;
import org.springframework.stereotype.Service;

@Service
public class PeopleServiceImpl extends ServiceImpl<PeopleDao, People> implements PeopleService {
	@Override
	public Page<People> selectVoList(Page<People> peoplePage, EntityWrapper<People> peopleQueryWrapper) {
		//若需要对查询出来的结果重新过滤并分页
		//只需要设置结果集，以及total总数据，会自动计算分页。
		//peoplePage.setRecords();
		//peoplePage.setTotal();

		return peoplePage.setRecords(this.baseMapper.selectVoList(peoplePage, peopleQueryWrapper));


	}

}
