
package com.minxin.modules.bz.dao;

import com.baomidou.mybatisplus.annotations.SqlParser;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.minxin.modules.bz.entity.People;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface PeopleDao extends BaseMapper<People> {

	/**
	 * 分页查询
	 *
	 * @param peoplePage         分页
	 * @param peopleQueryWrapper 查询条件
	 * @return
	 */
	//使用该注解，排除多租户查询
	@SqlParser(filter = true)
	List<People> selectVoList(Page<People> peoplePage, @Param("ew") EntityWrapper<People> peopleQueryWrapper);

}
