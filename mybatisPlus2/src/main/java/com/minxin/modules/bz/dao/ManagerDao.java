
package com.minxin.modules.bz.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.minxin.modules.bz.entity.Manager;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ManagerDao extends BaseMapper<Manager> {
	
}
