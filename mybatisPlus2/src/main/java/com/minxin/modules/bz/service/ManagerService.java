
package com.minxin.modules.bz.service;


import com.baomidou.mybatisplus.service.IService;
import com.minxin.modules.bz.entity.Manager;

public interface ManagerService extends IService<Manager> {

}
