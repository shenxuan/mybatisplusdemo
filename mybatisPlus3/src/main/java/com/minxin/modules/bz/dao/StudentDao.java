
package com.minxin.modules.bz.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.minxin.modules.bz.entity.Student;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface StudentDao extends BaseMapper<Student> {
	
}
