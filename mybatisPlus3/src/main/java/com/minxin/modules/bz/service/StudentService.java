
package com.minxin.modules.bz.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.minxin.modules.bz.entity.Student;

public interface StudentService extends IService<Student> {

}
